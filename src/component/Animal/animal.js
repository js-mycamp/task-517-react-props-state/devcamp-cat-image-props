import { Component } from "react";
import img from "../../assets/images/cat.jpg"

class Animal extends Component {
    render(){
        return(
            <>
            {this.props.kind ==="cat" ? <img src={img} alt="cat" /> : <p>meo not found</p>}
            </>
        )
    }
}

export default Animal
